@extends('layout.app')

@section('title','Edit Barang')

@section('body')

<form method="post" action="/barang/{{ $barang->id }}">
	{{ csrf_field() }}
  {{ method_field('PUT') }}
  <fieldset>
    <legend>Edit Barang</legend>
    <div class="form-group">
      <label for="kodebrg">Masukkan kode barang</label>
      <input type="text" name="kodebrg" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kode barang" value="{{ $barang->kodebrg }}">
    </div>
    <div class="form-group">
      <label for="namabrg">Masukkan nama barang</label>
      <input type="text" name="namabrg" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="nama barang" value="{{ $barang->namabrg }}">
    </div>
    <div class="form-group">
      <label for="exampleSelect1">Satuan barang</label>
      <select class="form-control" id="id_satuan" name="id_satuan">
        @foreach ($satuans as $satuan)
          @if ($satuan->id == $barang->id_satuan)
            <option value="{{ $satuan->id }}" SELECTED>{{ $satuan->satuan }}</option>
          @else
            <option value="{{ $satuan->id }}">{{ $satuan->satuan }}</option>
          @endif
        @endforeach
      </select>
    </div>
    <div class="form-group">
      <label for="hargabrg">Masukkan harga barang</label>
      <input type="text" name="hargabrg" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="harga barang" value="{{ $barang->hargabrg }}">
    </div>

    <button type="submit" class="btn btn-primary">Simpan</button>
    <button type="button" class="btn btn-primary" onclick="location.href='/barang'" >Kambali</button>
  </fieldset>
</form>

@endsection