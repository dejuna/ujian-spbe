@extends('layout.app')

@section('title','Tambah Barang')

@section('body')

<form method="post" action="/barang">
	{{ csrf_field() }}
  <fieldset>
    <legend>Tambah Barang Baru</legend>
    <div class="form-group">
      <label for="kodebrg">Masukkan kode barang</label>
      <input type="text" name="kodebrg" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kode barang">
    </div>
    <div class="form-group">
      <label for="namabrg">Masukkan nama barang</label>
      <input type="text" name="namabrg" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="nama barang">
    </div>
    <div class="form-group">
      <label for="exampleSelect1">Pilih satuan barang</label>
      <select class="form-control" id="id_satuan" name="id_satuan">
        @foreach ($satuans as $satuan)
          <option value="{{ $satuan->id }}">{{ $satuan->satuan }}</option>
          {{-- expr --}}
        @endforeach
      </select>
    </div>
    <div class="form-group">
      <label for="hargabrg">Masukkan harga barang</label>
      <input type="text" name="hargabrg" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="harga barang">
    </div>

    <button type="submit" class="btn btn-primary">Simpan</button>
    <button type="button" class="btn btn-primary" onclick="location.href='/barang'" >Kambali</button>
  </fieldset>
</form>

@endsection